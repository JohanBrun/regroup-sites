import React from 'react'

interface IBody{
    webSite: any
    setWebSite: any
}

const Body: React.FunctionComponent<IBody> = (props) => {

    return(
        <div className="DisplaySite">
            <iframe id={props.webSite}
                title={props.webSite}
                width="100%"
                height="100%"
                src={props.webSite} />
        </div>
    )
}

export default Body