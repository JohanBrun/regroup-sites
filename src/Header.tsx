import React from 'react'

interface IHeader{
    webSite: any
    setWebSite: any
}

const Header: React.FunctionComponent<IHeader> = (props) => {

    const handleClick = (url : string) => {
        props.setWebSite(url)
    }

    return(
        <div className="Title">
            <div className="webSiteButton" onClick={()=>handleClick("https://www.google.com/search?igu=1")}>google</div>
            <div className="webSiteButton" onClick={()=>handleClick("https://www.lemonde.fr/")}>lemonde</div>
            <div className="webSiteButton" onClick={()=>handleClick('https://www.gnoodiplo.net/')}>gnoodiplo</div>
            <div className="webSiteButton" onClick={()=>handleClick('https://www.youtube.com/')}>youtube</div>
        </div>
    )
}

export default Header