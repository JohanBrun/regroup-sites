import React from 'react';
import logo from './logo.svg';
import './App.css';
import Header from './Header';
import Body from './Body';

function App() {
  const [webSite, setWebSite] = React.useState('')

  return (
    <div className="App">
      <Header webSite={webSite} setWebSite={setWebSite}/>
      <Body webSite={webSite} setWebSite={setWebSite}/>
    </div>
  );
}

export default App;
